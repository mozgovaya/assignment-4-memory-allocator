#include "tests.h"
#include "mem.h"
#define SIZE_MEMORY_1 1330

void test1(void *heap_start) {
    puts("Тест1. Обычное успешное выделение памяти");
    void *allocated = _malloc(SIZE_MEMORY_1);
    if (allocated == NULL){
        puts("Ошибка. Память не выделилась");
    }
    debug_heap(stdout, heap_start);
    _free(allocated);
    debug_heap(stdout, heap_start);
    fprintf(stdout, "Успешно");
}

void test2(void *heap_start) {
    puts("Тест2. Освобождение одного блока из нескольких выделенных");
    void *allocated1 = _malloc(SIZE_MEMORY_1);
    void *allocated2 = _malloc(SIZE_MEMORY_1);
    if (allocated1 == NULL || allocated2 == NULL){
        puts("Ошибка. Память не выделилась");
    }
    _free(allocated1);
    debug_heap(stdout, heap_start);
    _free(allocated2);
    debug_heap(stdout, heap_start);
    fprintf(stdout, "Успешно");

}

void test3(void *heap_start) {
    puts("Тест3. Освобождение двух блоков из нескольких выделенных");
    void *allocated1 = _malloc(SIZE_MEMORY_1);
    void *allocated2 = _malloc(SIZE_MEMORY_1);
    void *allocated3 = _malloc(SIZE_MEMORY_1);
    void *allocated4 = _malloc(SIZE_MEMORY_1);
    if (allocated1 == NULL || allocated2 == NULL || allocated3 == NULL || allocated4 == NULL){
        puts("Ошибка. Память не выделилась");
    }
    _free(allocated1);
    _free(allocated2);
    debug_heap(stdout, heap_start);
    _free(allocated3);
    _free(allocated4);
    debug_heap(stdout, heap_start);
    fprintf(stdout, "Успешно");
}

void test4(void *heap_start) {
    puts("Тест4. Память закончилась, новый регион памяти расширяет старый");
    void *allocated = _malloc(100000000);
    if (allocated == NULL){
        puts("Ошибка. Память не выделилась");
    }

    debug_heap(stdout, heap_start);
    _free(allocated);
    debug_heap(stdout, heap_start);
}

void test5(void *heap_start) {
    puts("Тест5. Новый регион выделяется в другом месте");
    size_t heap_sz = heap_size(heap_start);
    debug_heap(stdout, heap_start);
    map_pages((uint8_t *) heap_start + heap_sz, 5000, 0x100000);
    debug_heap(stdout, heap_start);
    void *allocated = _malloc(heap_sz * 2);
    debug_heap(stdout, heap_start);
    _free(allocated);
    debug_heap(stdout, heap_start);
}

void run_all_tests() {
    void *heap_start = heap_init(10);
    test1(heap_start);
    test2(heap_start);
    test3(heap_start);
    test4(heap_start);
    test5(heap_start);
}
